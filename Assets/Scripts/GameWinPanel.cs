﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameWinPanel : MonoBehaviour
{
    [SerializeField]
    private Transform root;

    [SerializeField]
    private Text yourScoreText;

    [SerializeField]
    private Button nextLevelBtn, toMainMenuBtn;

    // Start is called before the first frame update
    void Start()
    {
        root.gameObject.SetActive(false);
        nextLevelBtn.onClick.AddListener(OnNextLevelClick);
        toMainMenuBtn.onClick.AddListener(OnToMainMenuClick);

    }

    public void SetPanelActive(bool state) // state для выкл false/true вкл
    {
        root.gameObject.SetActive(state);
    }

    public void SetScoreText(int yourScore)
    {
        yourScoreText.text = "Your score: " + yourScore;
    }

    private void OnToMainMenuClick()
    {
        SceneManager.LoadScene(0);
    }

    private void OnNextLevelClick()
    {
        SceneManager.LoadScene(2);
    }

    // Update is called once per frame
    private void OnDestroy()
    {
        nextLevelBtn.onClick.RemoveAllListeners();
        toMainMenuBtn.onClick.RemoveAllListeners();
    }
}
