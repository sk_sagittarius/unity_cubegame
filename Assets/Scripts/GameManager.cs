﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameOverPanel gameOverPanel;
    
    [SerializeField]
    private GameWinPanel gameWinPanel;

    public void WinGame(int score)
    {
        gameWinPanel.SetPanelActive(true);
        gameWinPanel.SetScoreText(score);
    }

    public void GameOver(int score)
    {
        gameOverPanel.SetPanelActive(true);
        gameOverPanel.SetScoreText(score);
    }

}
