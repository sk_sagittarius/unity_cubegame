﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI; // для UI элементов

public class Player : MonoBehaviour
{
    [SerializeField]
    private Rigidbody rb;

    [SerializeField]
    private float speed, sideSpeed, speedUp;
    private float delay = 3f;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private GameManager gameManager;
    bool isSpeedUp = false;

    private int coins = 0;
    private int coinsFirstLevel;

    
    // Start is called before the first frame update
    private void Start()
    {
        //rb = GetComponent<Rigidbody>(); // для тех элементов, которые рядом. Один из вариантов
        scoreText.text = "0";
    }

    private void Update()
    {
        rb.AddForce(0,0,speed * Time.deltaTime); // движение вперед

        if (Input.GetKey("a"))
        {
            rb.AddForce(-sideSpeed * Time.deltaTime,0,0, ForceMode.Impulse);
        }

        if (Input.GetKey("d"))
        {
            rb.AddForce(sideSpeed * Time.deltaTime,0,0, ForceMode.Impulse);
        }

        if(transform.position.y < -10)
        {
            gameManager.GameOver(coins);
        }    

        if(isSpeedUp == true)
        {
            if (delay > 0)
                {
                    Debug.Log("delay if " + delay);
                    delay -= Time.deltaTime;
                }
                else
                {
                    delay = 0;
                    Debug.Log("delay else " + delay);

                    speed = speed - speedUp;
                    rb.AddForce(0,0,speed * Time.deltaTime);
                    isSpeedUp = false;
                }
        } 
    }


    private void OnTriggerEnter(Collider other) 
    {
        Debug.Log("Trigger hit" + other.tag); // параметр имя доступен на уровень выше
        if (other.tag == "Coin")
        {
            
            coins++;
            Destroy(other.gameObject); // уничтожение другого объекта
            if(coins>=3)
            {
                speed += speedUp;
                rb.AddForce(0,0,speed * Time.deltaTime);
                
            }
        }
        scoreText.text = coins.ToString();
        
        
        if(other.tag == "SpeedUp")
        {
            isSpeedUp = true;
            Destroy(other.gameObject);

            speed += speedUp;
            rb.AddForce(0,0,speed * Time.deltaTime);
        }

        if(other.tag == "Finish")
        {
            gameManager.WinGame(coins);
            speed = 0;
            rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            
        }

    }


    private void OnCollisionEnter(Collision other) 
    {
        Debug.Log("Collision hit" + other.gameObject.name);
        if(other.gameObject.tag=="Block")
        {
            this.enabled = false;
            gameManager.GameOver(coins);
        }
    }
}
