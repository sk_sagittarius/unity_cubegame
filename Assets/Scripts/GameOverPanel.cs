﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField]
    private Transform root;

    [SerializeField]
    private Text yourScoreText, bestScoreText;

    [SerializeField]
    private Button restartBtn, exitBtn;


    private void Start()
    {
        root.gameObject.SetActive(false); // отключаем  панель на старте игры
        restartBtn.onClick.AddListener(OnRestartClick);
        exitBtn.onClick.AddListener(OnExitClick);

    }

    public void SetPanelActive(bool state) // state для выкл false/true вкл
    {
        root.gameObject.SetActive(state);
    }


    public void SetScoreText(int yourScore)
    {
        int bestScore = PlayerPrefs.GetInt("BestScore"); // player pref - Хранилище
        // код для обновления лучшего результата персонажа. Если меньше - вывести то, что уже хранится в реестре
        if(bestScore<yourScore)
        {
            bestScore = yourScore;
            PlayerPrefs.SetInt("BestScore", bestScore);
        }
        bestScoreText.text = "Best score: " + bestScore;
        yourScoreText.text = "Your score: " + yourScore;
    }

    private void OnRestartClick()
    {
        SceneManager.LoadScene(1);
    }

    private void OnExitClick()
    {
        Application.Quit();
    }

    private void OnDestroy()
    {
        restartBtn.onClick.RemoveAllListeners();
        exitBtn.onClick.RemoveAllListeners();
    }
}
