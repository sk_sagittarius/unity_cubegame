﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button startBtn, exitBtn;

    public void Start()
    {
        startBtn.onClick.AddListener(StartGame);
        exitBtn.onClick.AddListener(ExitGame);
    }

    private void StartGame()
    {
        SceneManager.LoadScene(1); // в скобках уровни
    }

        private void ExitGame()
    {
        Application.Quit();
    }

    private void OnDestroy()
    {
        startBtn.onClick.RemoveAllListeners();
        exitBtn.onClick.RemoveAllListeners();

    }
}
